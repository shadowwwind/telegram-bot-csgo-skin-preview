# Telegram Bot CSGO Skins Preview
[![Telegram Bot](https://img.shields.io/badge/-@SkinViewBot-blue?logo=telegram)](https://t.me/skinviewbot)
<br>
A Telegram bot that will reply with an swap.gg genrated screenshot to inspect links in dm's and groups. You can also use inline commands to send screenshots in any chat.

There is a public instance running at https://t.me/skinviewbot

# Self hosting
Go to https://t.me/botfather and create a bot.

Copy the API code and save it to .env at BOT_TOKEN=[here]

Go back to botfather, select the bot go to bot settings > inline mode and enable inline mode.

Run index.js