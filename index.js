const { Telegraf, Markup } = require('telegraf')
require('dotenv').config()
let Promise = require('bluebird');
let request = require('request-promise');
let io = require('socket.io-client');

let socket = io(process.env.SOCKET);

let screenshotQueue = {};

const bot = new Telegraf(process.env.BOT_TOKEN)

socket.on('screenshot:ready', (data) => {
    //console.log('\x1b[36m%s\x1b[0m', "screenshot created: "+data.marketName+" "+data.imageLink+" "+data.inspectLink)
    let inspectLink = data.inspectLink;
    let imageLink = data.imageLink;

    if (screenshotQueue[inspectLink] === undefined) {
        return;
    }

    let messages = screenshotQueue[inspectLink];

    for (let message of messages) {
        message.reply(imageLink);
        console.log("editmesgaetext")
    }

    if (screenshotQueue[inspectLink] === inspectLink) {
        ctx.editMessageText(messageId, imageLink)
        console.log("editmesgaetext")
    }

    console.log('\x1b[33m%s\x1b[0m', "screenshot:ready "+" "+data.marketName+" "+data.imageLink+" "+data.inspectLink)


    delete screenshotQueue[inspectLink];
});

bot.start((ctx) => {
    ctx.reply("This is a simple bot to generate high definition screenshots from csgo inspect links. You can send inspects links here and I will send the screenshot back or add me to do the same there. You can also use the inline mode every where by typing @SkinViewBot.")
})

bot.on("message", (ctx) => {
    messageContent = ctx.message.text
    messageId = ctx.message.message_id

    let inspectLink = parseLink(messageContent.replace(/%20/g, ' '));

    if (inspectLink === null) {
        return;
    }

    generateScreenshots(ctx, inspectLink);
})

bot.on( 'inline_query', (ctx)=> {
    console.log(ctx.inlineQuery.query)
    inlineContent = ctx.inlineQuery.query
    inlineID = ctx.inlineQuery.id
    results = ""

    let inspectLink = parseLink(inlineContent.replace(/%20/g, ' '));

    if (inspectLink === null) {
        results = [
            {
                type: 'article',
                id: '0',
                title: 'Help',
                message_text: 'Get help',
            },
            {
                type: 'photo',
                id: '1',
                title: 'Desert Eagle | Printstream (Factory New)',
                description: 'Float: 0.04506531 | Pattern: 36',
                caption: 'Desert Eagle | Printstream (Factory New) \n Float: 0.04506531 \n Pattern: 36',
                photo_width: '1920',
                photo_height: '1725',
                photo_url: 'https://s.swap.gg/m9LvxMe3n6.jpg',
                thumb_url: 'https://s.swap.gg/m9LvxMe3n6.jpg',
            },
            {
                type: 'photo',
                id: '2',
                title: '★ Karambit | Case Hardened (Factory New)',
                description: 'Float: 0.04948986 | Pattern: 149',
                caption: '★ Karambit | Case Hardened (Factory New) \n Float: 0.04948986 \n Pattern: 149',
                photo_width: '1920',
                photo_height: '1725',
                photo_url: 'https://s.swap.gg/Y3A8Js1J6p.jpg',
                thumb_url: 'https://s.swap.gg/Y3A8Js1J6p.jpg',
            },
            {
                type: 'photo',
                id: '3',
                title: 'AK-47 | Case Hardened (Factory New)',
                description: 'Float: 0.01583437 | Pattern: 851',
                caption: 'AK-47 | Case Hardened (Factory New) \n Float: 0.01583437 \n Pattern: 851',
                photo_width: '1920',
                photo_height: '1725',
                photo_url: 'https://s.swap.gg/YrMvWQ4TYC.jpg',
                thumb_url: 'https://s.swap.gg/YrMvWQ4TYC.jpg',
            },
            {
                type: 'photo',
                id: '4',
                title: 'AWP | Asiimov (Field-Tested)',
                description: 'Float: 0.33508345 | Pattern: 252',
                caption: 'AWP | Asiimov (Field-Tested) \n Float: 0.33508345 \n Pattern: 252',
                photo_width: '1920',
                photo_height: '1725',
                photo_url: 'https://s.swap.gg/meYnQDO5mz.jpg',
                thumb_url: 'https://s.swap.gg/meYnQDO5mz.jpg',
            },
            {
                type: 'photo',
                id: '5',
                title: '★ Classic Knife | Fade (Factory New)',
                description: 'Float: 0.00000005 | Pattern: 141',
                caption: '★ Classic Knife | Fade (Factory New) \n Float: 0.00000005 \n Pattern: 141',
                photo_width: '1920',
                photo_height: '1725',
                photo_url: 'https://s.swap.gg/D1Md3bWpKBC12MSMOitIo.jpg',
                thumb_url: 'https://s.swap.gg/D1Md3bWpKBC12MSMOitIo.jpg',
            }
        ]
    }
    console.log(results)

    ctx.answerInlineQuery(results)
})

console.log("Started")
bot.launch()

function parseLink(content) {
    let matches = content.match(/^steam:\/\/rungame\/730\/\d+\/[+ ]csgo_econ_action_preview [SM]\d+A\d+D\d+/mg);

    if (matches !== null) {
        console.log(matches)
        return matches
    }else {

    return null;
    }
}

function generateScreenshots(ctx, inspectLink) {
    let creatingScreenshot = false;
    let errorMessages = [];

    return Promise.each(inspectLink, (inspectLink) => {
        return new Promise((resolve) => {
            return request({
                method: 'POST',
                url: 'https://market-api.swap.gg/v1/screenshot',
                json: true,
                body: {
                    inspectLink: inspectLink
                }
            }).then((data) => {
                let result = data.result;
                console.log(JSON.stringify(result))

                let id = result.requestId;
                let inspectLink = result.inspectLink;
                let state = result.state;

                console.log("ID: "+id)

                if (state === 'IN_QUEUE') {
                    console.log(state)
                    ctx.replyWithMarkdown('Creating Screenshot for *'+result.marketName+"*...");
                    if (screenshotQueue[inspectLink] === undefined) {
                        screenshotQueue[inspectLink] = [];
                    }

                    screenshotQueue[inspectLink].push(message)

                    creatingScreenshot = true;
                } else if (state === 'COMPLETED') {
                    replyText = '<a href="'+result.imageLink+'">'+result.marketName+'</a>';
                    ctx.replyWithHTML(replyText)
                }

                return resolve();
            }).catch((err) => {
                errorMessages.push(err.error.status);

                return resolve();
            });
        });
    })
}